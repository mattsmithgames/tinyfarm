﻿namespace Sirenix.OdinInspector.Demos
{
    using UnityEngine;
    using System.Collections.Generic;

    public class ListExamples : SerializedMonoBehaviour
    {
        [InfoBox("Hold ctrl while dragging an element to make a copy")]
        public List<float> FloatArray = new List<float>();

        [Range(0, 1)]
        public float[] FloatRangeList;

        public List<GameObject> GameObjectList;

        public List<Transform> TransformList;

        /// <summary>
        /// In order to serialize the object array, all we need to do is to inherit from SerializedMonoBehaviour.
        /// </summary>
        public object[] ObjectArray;
    }
}