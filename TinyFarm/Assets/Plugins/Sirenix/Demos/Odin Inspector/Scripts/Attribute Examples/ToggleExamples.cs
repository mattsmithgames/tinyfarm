﻿namespace Sirenix.OdinInspector.Demos
{
    using System;
    using UnityEngine;

    public class ToggleExamples : MonoBehaviour
    {
        // Toggle for custom data.
        [ToggleGroup("EnableGroupOne", order: -1, titleStringMemberName: "GroupOneTitle")]
        public bool EnableGroupOne;

        [ToggleGroup("EnableGroupOne")]
        public string GroupOneTitle = "One";

        [ToggleGroup("EnableGroupOne")]
        public float GroupOneA;

        [ToggleGroup("EnableGroupOne")]
        public float GroupOneB;

        // Group two.
        [ToggleGroup("EnableGroupTwo", order: -1, titleStringMemberName: "GroupTwoTitle")]
        public bool EnableGroupTwo;

        [ToggleGroup("EnableGroupOne")]
        public string GroupTwoTitle = "Two";

        [ToggleGroup("EnableGroupTwo")]
        public string GroupTwoString;

        // Toggle for individual objects.
        [Title("Toggle objects")]
        [Toggle("Enabled")]
        public MyToggleObject Three = new MyToggleObject();

        [Toggle("Enabled")]
        public MyToggleObject Four = new MyToggleA();

        [Toggle("Enabled")]
        public MyToggleObject Five = new MyToggleB();
    }

    [Serializable]
    public class MyToggleObject
    {
        // The toggle attributes find this member and use that as the toggle.
        [HideInInspector]
        public bool Enabled;

        [HideInInspector]
        public string Title;

        public int A;
        public int B;
    }

    [Serializable]
    public class MyToggleA : MyToggleObject
    {
        public float C;
        public float D;
        public float F;
    }

    [Serializable]
    public class MyToggleB : MyToggleObject
    {
        public string Text;
    }
}