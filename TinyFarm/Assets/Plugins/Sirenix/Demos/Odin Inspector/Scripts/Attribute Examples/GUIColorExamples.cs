﻿namespace Sirenix.OdinInspector.Demos
{
    using UnityEngine;

    public class GUIColorExamples : MonoBehaviour
    {
        [GUIColor(1f, 0f, 0f)]
        public int A;

        [GUIColor(1f, 0.5f, 0f)]
        public int B;

        [GUIColor(1f, 1f, 0f)]
        public int C;

        [GUIColor(0f, 1f, 0f)]
        public int D;

        [GUIColor(0f, 1f, 1f)]
        public int E;

        [GUIColor(0f, 0f, 1f)]
        public int F;

        [GUIColor(1f, 0f, 1f)]
        public int G;
    }
}