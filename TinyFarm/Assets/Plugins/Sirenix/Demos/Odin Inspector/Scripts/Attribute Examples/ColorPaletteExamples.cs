﻿namespace Sirenix.OdinInspector.Demos
{
    using UnityEngine;

    public class ColorPaletteExamples : MonoBehaviour
    {
        [ColorPalette("Underwater")]
        public Color UnderwaterColor;

        [ColorPalette("Unknown Color Palette")]
        public Color MyColor1;

        [ColorPalette]
        public Color MyColor2;

        [ColorPalette("Fall")]
        public Color[] ColorArray;
    }
}