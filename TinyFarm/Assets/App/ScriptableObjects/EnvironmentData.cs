﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class EnvironmentData : ScriptableObject {

    public List<GameObject> Trees = new List<GameObject>();

    public List<GameObject> Rocks = new List<GameObject>();

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject RandomRock()
    {
        int i = Random.Range(0, Rocks.Count);

        return Rocks[i];
    }

    public GameObject RandomTree()
    {
        int i = Random.Range(0, Trees.Count);

        return Trees[i];
    }
}
