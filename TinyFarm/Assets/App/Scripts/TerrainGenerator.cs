﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class TerrainGenerator : MonoBehaviour {

    public EnvironmentData EnvironmentDatabase;

    [ReadOnly]
    [SerializeField]
    List<GameObject> Objects = new List<GameObject>();

    int Range = 240;

    public float TreeDensity;
    public float RockDensity;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [Button("Generate")]
    public void GenerateEnvironment()
    {
        ClearEnvironment();
        GenerateRocks();
        GenerateTrees();
    }

    [Button("Clear")]
    public void ClearEnvironment()
    {
        foreach(GameObject go in Objects)
        {
            GameObject.DestroyImmediate(go);

        }

        Objects.Clear();
    }

    private void GenerateTrees()
    {
        int tMin = Mathf.RoundToInt(TreeDensity / 2f);

        int tCount = Random.Range(tMin, (int)TreeDensity);

        for (int i = 0; i < tCount; i++)
        {
            GameObject tree = EnvironmentDatabase.RandomTree();
            Spawn(tree);
        }
    }

    private void GenerateRocks()
    {
        int rMin = Mathf.RoundToInt(RockDensity / 2f);

        int rCount = Random.Range(rMin, (int)RockDensity);

        for (int i = 0; i < rCount; i++)
        {
            GameObject rock = EnvironmentDatabase.RandomRock();
            Spawn(rock);
        }
    }

    private GameObject Spawn(GameObject go)
    {
        GameObject spawned = (GameObject)Instantiate(go, this.transform);
        spawned.transform.eulerAngles = new Vector3(0, Random.Range(0f, 360f), 0);
        float x = Random.Range(-120f, 120f);
        float y = Random.Range(-120f, 120f);

        float yScale = Random.Range(1f, 1.4f);
        float xScale = Random.Range(1f, 1.4f);

        spawned.transform.localPosition = new Vector3(x, 3.4f, y);
        spawned.transform.localScale = new Vector3(xScale, yScale, xScale);

        Objects.Add(spawned);
        

        return spawned;
    }
}
